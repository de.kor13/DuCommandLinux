#include <iostream>
#include <vector>
#include <string>
#include <filesystem>
#include <fstream>
#include <boost/program_options.hpp>

namespace fs = std::filesystem;
namespace po = boost::program_options;
uintmax_t totalSize = 0;

struct FileInfo {
  std::string path;
  uintmax_t size;
  bool isDirectory;
  bool isDirEmpty;
};

void processDirectory(const std::string & path, std::vector < FileInfo > & files, std::vector < std::string > & processedPaths) {

  if (std::find(processedPaths.begin(), processedPaths.end(), path) != processedPaths.end()) {
    return;
  }

  processedPaths.push_back(path);

  uintmax_t size = 0;

  for (const auto & entry: fs::directory_iterator(path)) {
    if (entry.is_directory()) {
      processDirectory(entry.path().string(), files, processedPaths);
      if (fs::directory_iterator(entry.path()) == fs::directory_iterator {})
        files.push_back({
          entry.path().string(),
          0,
          true,
          true
        });
      else
        files.push_back({
          entry.path().string(),
          0,
          true,
          false
        });

    } else {
      size += entry.file_size();
      files.push_back({
        entry.path().string(),
        entry.file_size(),
        false,
        true
      });
    }
  }

}

void printFiles(const std::vector < FileInfo > & files, bool showAll, bool inBytes, bool showSizeOnly, bool showTotal,
  const std::string & dirPath) {
  uintmax_t totalSize_byte = 0;
  uintmax_t totalSize_batch = 0;

  for (const auto & file: files) {
    if (file.isDirectory) {
      totalSize_batch += 1;
      if (!showSizeOnly) {
        if (file.isDirEmpty)
          std::cout << (inBytes ? 0 : 1) << "\t" << file.path << std::endl;
        else
          std::cout << (inBytes ? totalSize_byte : totalSize_batch) << "\t" << file.path << std::endl;
      }
    } else {
      totalSize_byte += file.size;
      totalSize_batch += (file.size + 511) / 512;
      if (showAll) {
        std::cout << (inBytes ? file.size : (file.size + 511) / 512) << "\t" << file.path << std::endl;
      }
    }

  }

  if (showSizeOnly) { // -s
    if (!dirPath.empty()) {
      std::cout << (inBytes ? totalSize_byte : totalSize_batch) << "\t" << dirPath << std::endl;
    } else {
      std::cout << (inBytes ? totalSize_byte : totalSize_batch) << std::endl;
    }
  }

  if (showTotal) { // -c 
    inBytes ? totalSize += totalSize_byte : totalSize += totalSize_batch;
  }
}

int main(int argc, char * argv[]) {
  std::vector < std::string > paths;
  std::vector < std::string > filesFrom;
  bool showAll = false;
  bool showTotal = false;
  bool inBytes = false;
  bool showSizeOnly = false;

  po::options_description desc("Options");
  desc.add_options()
    ("help,h", "Print help messages")
    ("all,a", "Show all files")
    ("bytes,b", "Print sizes in bytes")
    ("total,c", "Print total size in the end")
    ("size-only,s", "Print only total size")
    ("files-from,f", po::value < std::vector < std::string >> ( & filesFrom), "Read list of paths from file")
    ("path", po::value < std::vector < std::string >> ( & paths), "List of paths to process");

  po::positional_options_description positional;
  positional.add("path", -1);

  po::variables_map vm;

  try {
    po::store(po::command_line_parser(argc, argv).options(desc).positional(positional).run(), vm);

    if (vm.count("help")) {
      std::cout << desc << std::endl;
      return 0;
    }

    if (vm.count("all")) {
      showAll = true;
    }

    if (vm.count("bytes")) {
      inBytes = true;
    }

    if (vm.count("total")) {
      showTotal = true;
    }

    if (vm.count("size-only")) {
      showSizeOnly = true;
    }

    po::notify(vm);
  } catch (const po::error & e) {
    std::cerr << "Error: " << e.what() << std::endl;
    std::cerr << desc << std::endl;
    return 1;
  }

  std::vector < std::string > pathsToProcess;

  if (!filesFrom.empty()) {
    for (const auto & file: filesFrom) {
      std::ifstream input(file);

      if (!input.good()) {
        std::cerr << "Error: cannot read file " << file << std::endl;
        return 1;
      }

      std::string line;
      while (std::getline(input, line)) {
        pathsToProcess.push_back(line);
      }
    }
  } else {
    pathsToProcess = paths;
  }

  for (const auto & path: pathsToProcess) {
    std::vector < FileInfo > files;
    std::vector < std::string > processedPaths;
    if (path.find('*') != std::string::npos) {
      for (const auto & entry: fs::recursive_directory_iterator(fs::path(path).parent_path())) {
        if (entry.path().string().find(path) != std::string::npos) {
          if (entry.is_directory()) {

            processDirectory(entry.path().string(), files, processedPaths);
          } else {
            files.push_back({
              entry.path().string(),
              entry.file_size(),
              false
            });
          }
        }
      }
    } else {
      if (fs::is_directory(path)) {
        processDirectory(path, files, processedPaths);
      } else {
        files.push_back({
          path,
          0,
          true,
          false
        });
      }
    }
    files.push_back({
      path,
      0,
      true
    });
    printFiles(files, showAll, inBytes, showSizeOnly, showTotal, path);
  }
  if (showTotal) {
    std::cout << "Total size: " << totalSize << std::endl;
  }

  return 0;
}