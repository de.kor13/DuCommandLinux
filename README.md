# du command

## what does the program do?


analogue of the du command (as in Linux)


## build
CMAKE VERSION 3.15.0<br>
use c++ 17, std::filesystem<br>
Use boost version 1.71.0 COMPONENTS program_options(командной строкой)<br>
sudo apt-get install libboost-program-options1.71.0<br> 


mkdir build && cd build<br>
cmake .. -DCMAKE_BUILD_TYPE=Debug -DCPACK_GENERATOR=DEB -DCMAKE_PREFIX_PATH=/usr/lib && make package<br>

./du -c /home/user/testDU /home/user/ForWork/Fink<br>
Можно считывать директории из файла:<br>
./du -a -b -f file_txt.txt<br>
